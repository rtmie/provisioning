variable "count" {}

variable "token" {}

variable "domain" {}

variable "hostnames" {
  type = "list"
}

variable "public_ips" {
  type = "list"
}

provider "name.com" {
  token = "${var.token}"
}

resource "name.com_record" "hosts" {
  count = "${var.count}"

  domain = "${var.domain}"
  name   = "${element(var.hostnames, count.index)}"
  value  = "${element(var.public_ips, count.index)}"
  type   = "A"
  ttl    = 300
}

resource "name.com_record" "domain" {
  domain = "${var.domain}"
  name   = "@"
  value  = "${element(var.public_ips, 0)}"
  type   = "A"
  ttl    = 300
}

resource "name.com_record" "wildcard" {
  depends_on = ["name.com_record.domain"]

  domain = "${var.domain}"
  name   = "*.${var.domain}."
  value  = "@"
  type   = "CNAME"
  ttl    = 300
}

output "domains" {
  value = ["${name.com_record.hosts.*.fqdn}"]
}
